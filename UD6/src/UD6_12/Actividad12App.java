package UD6_12;

import javax.swing.JOptionPane;

public class Actividad12App {

	public static void main(String[] args) {
		String midaArrayUno = JOptionPane.showInputDialog(null,"Introduce el tama�o del array" );
		int midaArrayUnoInt = Integer.parseInt(midaArrayUno); 
		int arrayUno[]= new int [midaArrayUnoInt];
		String valorFinal;
		int valorFinalInt;
		do {
			 valorFinal = JOptionPane.showInputDialog(null,"Introduce el valor en el cual quieres que los numeros acaben,debe estar entre 0 y 9" );
			 valorFinalInt = Integer.parseInt(valorFinal);
		}while(valorFinalInt > 9 || valorFinalInt < 0); // Hacemos un comprobante para que no se salga de 0 y 9
		
		
		
		rellenarArrayRandom(arrayUno); // Rellenamos el array
		
		int numeroAcabadoArray[]= encontrarNumeroAcabado(arrayUno,valorFinalInt); // Y hacemos que el array de los numeros acabados en el valor seleccionasdo
																				  // Tenga el valor del metodo que encuentre los numeros
		
		// Mostramos el array
		mostrarArray(numeroAcabadoArray);
		
		
	}
	
	
	
	
	
	
	
	
	public static int[]  rellenarArrayRandom(int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] =  generarRandom(1,300);
		} // Metodo para rellenar
		return array;
	
	
	
	}
	private static int generarRandom(int numInicial, int numFinal) {
		int numeroRandom = (int) ((Math.random() * (300 -1)) + 1);
		return numeroRandom;
	}// Metodo para generar random
	
	
	
	
	public static int[] encontrarNumeroAcabado (int array[], int numeroAcabado) {
		int arrayAcabados[] = new int[array.length];
		
		int comprobanteIgual = 0;
		
		for (int i = 0; i < arrayAcabados.length; i++) {
			
			comprobanteIgual=array[i]-(array[i]/10*10); // Lo que hace es aislar en decimal y eliminar el valor para que quede acabado en 0 y restarselo al original para aislar el ultimo valor.
			
			
			if (comprobanteIgual == numeroAcabado) { // Si es igual lo a�adimos al array
				arrayAcabados[i] = array[i];
				
			}
			
			
			
			
		}
		
		
		
		return arrayAcabados;
	}
	
	
	public static void mostrarArray(int [] array) {
	
		for (int i = 0; i < array.length; i++) {
			if(array[i] > 0) { // Si el valor no es igual que el que se pide no hace falta mostrarlo
				System.out.println("Posicion " + i + " : " + array[i]);
			}

		}
		
	}
	
	

}
