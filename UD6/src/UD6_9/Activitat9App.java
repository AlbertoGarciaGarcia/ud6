package UD6_9;

import javax.swing.JOptionPane;

public class Activitat9App {

	public static void main(String[] args) {
		String midaArray = JOptionPane.showInputDialog(null,"Introduce el tama�o del array" );
		int midaArrayInt = Integer.parseInt(midaArray); 
		int[] array = new int [midaArrayInt];
		int numInicial = 0;
		int numFinal = 10;
		
		rellenarArrayRandom(array,numInicial,numFinal); // Llamamos a las funciones
		mostrarArray(array);
		
		
		
		
	}

	private static int generarRandom(int numInicial, int numFinal) { // Hacemos un metodo para generar un numero aleatorio entre el rango pedido por el usuario
		int numeroRandom = (int) ((Math.random() * (numFinal - numInicial)) + numInicial);
		return numeroRandom;
	}
	
	
	
	
	
	public static int[]  rellenarArrayRandom(int[] array, int numInicial, int numFinal) { // Rellenamos el array concuerdo al rango indicado
		for (int i = 0; i < array.length; i++) {
			array[i] =  generarRandom(numInicial,numFinal);
		}
		
		return array;
	}
	
	public static void mostrarArray(int [] array) { // Mostramos el array y hacemos al suma
		int total = 0;
		for (int i = 0; i < array.length; i++) {
			
			System.out.println("Posicion " + i + " : " + array[i]);
			total = total + array[i];
					
			
		}
		System.out.print(" El total de la suma es " + total);
	}
	

	
	
}
