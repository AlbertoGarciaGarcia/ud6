package UD6_2;

import javax.swing.JOptionPane;

public class Activitat2App {

	public static void main(String[] args) {
	
		String numero = JOptionPane.showInputDialog(null,"Introduce la cantidad de numeros que quieres generar");
		int cantidadNumero = Integer.parseInt(numero);   // Pedimos la cantidad de numeros
		String rangoInicial = JOptionPane.showInputDialog(null,"Introduce el rango inicial");
		int rangoInicialInt = Integer.parseInt(rangoInicial);   // Pedimos el rango inicial
		String rangoFinal = JOptionPane.showInputDialog(null,"Introduce el rango final");
		int rangoFinalInt = Integer.parseInt(rangoFinal);  // Y el rango final
		for (int i = 0; i < cantidadNumero; i++){
		numeroAleatorio(rangoInicialInt,rangoFinalInt);
		}

	}
	 
	public static int numeroAleatorio( int rangoInicial, int rangoFinal) {
		
		int numeroRandom = 0;
			numeroRandom = (int) ((Math.random() * (rangoFinal - rangoInicial)) + rangoInicial); // Simplemente generaremos un numero random, el cual devolveremos
			System.out.println(numeroRandom);													// teniendo en cuenta los rangos
		return numeroRandom;
		
		
	}

}
