package UD6_5;



import javax.swing.JOptionPane;

public class Activitat5App {

	public static void main(String[] args) {

	    	String numeroDecimal; 
			int numeroDecimalInt;
			do {
				numeroDecimal = JOptionPane.showInputDialog(null,"Introduce el numero que quieres convertir a binario");
				numeroDecimalInt = Integer.parseInt(numeroDecimal);
			}while(numeroDecimalInt< 0);
			
			
			JOptionPane.showMessageDialog(null,"El numero pasado a binario es " + calculoBinario(numeroDecimalInt));

	    
		
			
			
			
	    }
	public static String calculoBinario(int numero) {
		StringBuilder binario = new StringBuilder(); // Construiremos con el String builder el valor del binario que tendremos
 		while (numero > 0) { // Mientras el numero que se ira dividiendo sea menor que 0...
			int residuo = (int) (numero % 2); // Haremos que se cree una variable que coja el residuo de la divsion entre 2
			numero = numero/ 2; // Reduciremos el numero
			

			binario.insert(0, String.valueOf(residuo)); // E introduciremos el residuo
		}
		
		
		return binario.toString();
	}

	}


