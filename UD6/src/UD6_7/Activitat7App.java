package UD6_7;

import javax.swing.JOptionPane;

public class Activitat7App {

	public static void main(String[] args) {
		 String euros = JOptionPane.showInputDialog(null,"Introduce la cantidad de euros a convertir");
			double eurosDouble = Double.parseDouble(euros); 
			String divisa;
			
			do {
				divisa = JOptionPane.showInputDialog(null,"Introduce la divisa, recuerda, solo podemos convertir Libras,Dolares, o Yenes");
			}while(!divisa.equals("Dolares")&&!divisa.equals("Libras")&&!divisa.equals("Yenes")); // Haremos un comprobante para saber si el usuario introduce el valor adecuado
		
		convertidorDivisas(eurosDouble,divisa); // Llamos a la funcion
		
	}
	public static void convertidorDivisas(double euros, String divisa) {
		switch (divisa) {
		case "Libras":
			euros = euros * 0.86; // Hacemos la conversion e imprimimos
			JOptionPane.showMessageDialog(null,"Los euros convertidos a libras son  " + euros);
			break;
		case "Dolares":
			euros = euros * 1.28;
			JOptionPane.showMessageDialog(null,"Los euros convertidos a dolares son  " + euros);
			break;
		case "Yenes":
			euros = euros * 129.852;
			JOptionPane.showMessageDialog(null,"Los euros convertidos a Yenes son  " + euros);
			break;
		default:
			break;
		}
	}

}
