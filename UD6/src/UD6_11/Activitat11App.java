package UD6_11;

import javax.swing.JOptionPane;

public class Activitat11App {

	public static void main(String[] args) {
		String midaArrayUno = JOptionPane.showInputDialog(null,"Introduce el tama�o del primer array" );
		int midaArrayUnoInt = Integer.parseInt(midaArrayUno); 
		int arrayUno[]= new int [midaArrayUnoInt];	// Declaramos las variables necesarias
		String inicialRango = JOptionPane.showInputDialog(null,"Introduce el rango inicial" );
		int inicialRangoInt = Integer.parseInt(inicialRango); 
		String finalRango = JOptionPane.showInputDialog(null,"Introduce el rango final" );
		int finalRangoInt = Integer.parseInt(finalRango);
		
		
		
		rellenarArrayRandom(arrayUno,inicialRangoInt,finalRangoInt); // Aqui rellenaremos el array
		int arrayDos[];
		arrayDos=arrayUno; // Ahora asignamos el valor del dos al del uno
		arrayUno = new int[midaArrayUnoInt]; // Reiniciamos el uno
		rellenarArrayRandom(arrayUno,inicialRangoInt,finalRangoInt); // Lo volvemos a rellenar
		int arrayTres[]=multiplicarArrays(arrayUno,arrayDos); // Y hacemos que el array tres sea la multiplicacion del uno y del dos con un metodo
	
	
		
		
		System.out.println(" El contenido del primer array es ");
		mostrarArray(arrayUno);
		System.out.println(" El contenido del segundo array es "); // Mostramos los arrays
		mostrarArray(arrayDos);
		System.out.println(" El contenido del tercer array es ");
		mostrarArray(arrayTres);

	}

	
	
	public static int[]  rellenarArrayRandom(int[] array, int numInicial, int numFinal) {
		for (int i = 0; i < array.length; i++) {
			array[i] =  generarRandom(numInicial,numFinal);
		}
		return array;
	// Metodo para rellenar
	
	
	}
	
	
	
	
	private static int generarRandom(int numInicial, int numFinal) {
		int numeroRandom = (int) ((Math.random() * (numFinal - numInicial)) + numInicial);
		return numeroRandom;
	}
	
	// Metodo para genear random
	
	public static void mostrarArray(int [] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Posicion " + i + " : " + array[i]);
		}
	
	}
	 public static int[] multiplicarArrays(int array1[], int array2[]){
	        int array3[]=new int[array1.length];
	        for(int i=0;i<array1.length;i++){
	            array3[i]=array1[i]*array2[i];
	        }
	        return array3;
	    }
	// Metodo para multiplicar los valores del uno con los valores del dos 
	
		
	
}
