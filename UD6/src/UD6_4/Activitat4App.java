package UD6_4;

import javax.swing.JOptionPane;

public class Activitat4App {

	public static void main(String[] args) {
		String numeroFactorial = JOptionPane.showInputDialog(null,"Introduce el numero que quieres factorizar");
		int factorialInt = Integer.parseInt(numeroFactorial); // Pediremos el numero
		
		JOptionPane.showMessageDialog(null,"El numero factorizado es " + calculoFactorial(factorialInt));
		

	}

	public static int calculoFactorial(int n) {
        int resultado = 1;
        
        for (int i = 1; i <= n; i++) {
        	
            resultado *= i; // Haremos que multiplique tantas veces como valores anteriores sea
        }
        return resultado;
    }
	
}
