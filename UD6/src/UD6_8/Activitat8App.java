package UD6_8;

import javax.swing.JOptionPane;

public class Activitat8App {

	public static void main(String[] args) {
		int[] array = new int [10];
		rellenarArray(array); // LLamamos a las funcioens para rellenar el array y posteriormente mostrarlo
		mostrarArray(array);

	}
	
	public static int[] rellenarArray(int[] array) {
	
		for (int i = 0; i < array.length; i++) {
			 String valor = JOptionPane.showInputDialog(null,"Introduce el valor de la posicion " + i); // Pediremos al usuario que rellene el valor I 
				int valorInt = Integer.parseInt(valor); 
			array[i] =  valorInt;
			
		}
		
		return array;
		
	}
	
	public static void mostrarArray(int [] array) {
		for (int i = 0; i < array.length; i++) {
	
			System.out.println("Posicion " + i + " : " + array[i]); // Un for que por cada posicion del array imprima para visualiar cel contenido
		}
	}

}
